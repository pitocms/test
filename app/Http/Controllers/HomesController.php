<?php 

namespace App\Http\Controllers;

class HomesController extends Controller{
    
    public function getIndex(){
        return view('home.index');
    }
}